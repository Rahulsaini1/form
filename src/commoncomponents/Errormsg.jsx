

import { Typography } from "@material-ui/core";
import React from "react";


const Errormsg = (props) => {

    const { msg } = props;

    return (

        <Typography>{msg}</Typography>
    );
}
export default Errormsg;