import { TextField, Grid, Typography } from '@material-ui/core';
import React, { useRef, useState } from 'react';

import { Button, Container } from "@material-ui/core";
import { makeStyles } from '@material-ui/styles';



const useStyles = makeStyles((theme) => ({


    main: {
        height: '100vh',
        width: '100vw',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: ' rgb(38, 46, 51)',

    },
    container: {
        backgroundColor: 'rgb(61,72,79)',
        height: '65vh',
        borderRadius: '10px',


    }


}))





const Signup = () => {

    const nameRef = useRef(null);
    const emailRef = useRef(null);
    const passwordRef = useRef(null);

    const [email, setEmail] = useState(false);

    function submit(e) {
        e.preventDefault();

        localStorage.setItem('userName', JSON.stringify(nameRef.current.value));
        localStorage.setItem('userPassword', JSON.stringify(passwordRef.current.value))
        console.log(emailRef.current.value);
        const emailVal = emailRef.current.value;
        var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

        if (mailformat.test(emailVal)) {
            localStorage.setItem('userEmail', JSON.stringify(emailVal))

        } else {
            setEmail(true);
            console.log(emailVal)

        }

    }
    const classes = useStyles();

    return (
        <div className={classes.main}>

            <Container maxWidth="xs" className={classes.container}>
                <Typography style={{ marginTop: '100px' }} variant="h3" > Sign Up here.</Typography><br />



                <p>  {
                    email ? "error" : null
                }   </p>

                <form action=""
                    onSubmit={submit}>
                    <Grid container spacing={2}>


                        <Grid item xs={12} sm={12}  >
                            <TextField
                                label="Name"
                                variant="outlined"
                                placeholder="Enter Name"
                                autoComplete="off"
                                inputRef={nameRef}
                                required
                                fullWidth
                                color="secondary"
                            />
                        </Grid>
                        <Grid item xs={12} sm={12}  >
                            <TextField
                                label="Email"
                                variant="outlined"
                                placeholder="Enter email"
                                autoComplete="off"
                                inputRef={emailRef}
                                color="secondary"
                                required
                                fullWidth
                            />
                        </Grid>
                        <Grid item xs={12} sm={12}  >
                            <TextField
                                label="Password"
                                variant="outlined"
                                placeholder="Enter Password"
                                autoComplete="off"
                                inputRef={passwordRef}
                                color="secondary"
                                required
                                fullWidth
                            />
                        </Grid>


                        <Grid item xs={12} sm={12}  >

                            <Button fullWidth variant="contained" type="submit" >submit</Button>
                        </Grid>
                    </Grid>

                </form>
            </Container>
        </div>



    );
}
export default Signup