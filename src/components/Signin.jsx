import React, { useState } from "react";
import { Button, FormControlLabel, Checkbox, Container, TextField, Grid, Paper, makeStyles, Link, Typography,  } from "@material-ui/core";
import LockOpenIcon from '@material-ui/icons/LockOpen';
import '../Styles/Slider.css';
import '../commoncomponents/Errormsg'
import Errormsg from "../commoncomponents/Errormsg";
import { FcGoogle } from 'react-icons/fc';
import { useHistory } from "react-router-dom";
import Dashboard from "./Dashboard";
import Slider from './Slider'


const useStyles = makeStyles((theme) => ({


  main: {
    backgroundColor: ' rgb(38, 46, 51)',
    height: '100vh',
    width: '100vw',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'relative',
    flexDirection:'column'
  },

  container: {
    backgroundColor: 'rgb(61,72,79)',
    height: '65vh',
    borderRadius: '10px',
    boxShadow: '10px 10px 2px rgb(185, 60, 60)',
    [theme.breakpoints.down('xs')]: {
      width: '360px',
      height: '80vh',
      boxShadow: 'none',
    }


  },
  shape: {
    borderRadius: '50%',
    backgroundColor: 'rgb(38, 46, 51)',
    height: '30vh',
    width: '30vh',
    position: 'absolute',
    opacity: '0.1',
    marginBottom: '330px'

  },
  heading: {
    marginTop: '50px',
    fontSize: '3rem',
    fontWeight: 'bolder',
    fontFamily: 'sans-serif',
    [theme.breakpoints.down('xs')]: {
      marginTop: '20px',
    },

  },
  errorEmailText: {
    color: 'red',
    textShadow: 'none',
    fontFamily: 'sans-serif',


  },
  allRight: {
    marginTop: '100px',
    height:'1px',
    [theme.breakpoints.down('xs')]: {
      display: 'none'
    },

  },
  inptField: {
    color: '#ff0000',
    marginTop: '10px',
    borderRadius: '20px',

  },
  NotRegBtn: {
    textTransform: 'none',
    fontSize: '16px'
  },

  btnColour: {
    color: 'rgb(185, 60, 60)',
    marginTop: '8px',
    fontWeight: 'bolder',
    fontFamily: 'sans-serif',
    backgroundColor: 'black',
    boxShadow: '0 0 0 0 red',
    transition: 'ease-out 0.4s',
    textTransform: 'none',
    [theme.breakpoints.down('xs')]: {
      marginTop: '2px',
    },
    '&:hover': {
      boxShadow: '7px 7px 0 0 red',
      backgroundColor: 'red',
      color: 'black'
    },


  }
}));



const Signin = () => {
  const [credential, setCredential] = useState({
    email: '',
    password: ''
  })
  const [emailError, setEmailError] = useState(false);
  const [checked, setChecked] = useState(true);
  const [credentialerror, setCredentialError] = useState(false)


  const handleChange = e => {
    false
    setChecked(e.target.checked);
  };

  const login = e => {
    const { name, value } = e.target;
    setCredential({
      ...credential,
      [name]: value
    })

  }
  // ======== validate Email===========================================================
  const validateEmail = e => {
    const { name, value } = e.target;
    const email = value;
    var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (mailformat.test(email)) {
      setEmailError(false)
    }
    else {
      setEmailError(true)
    }

  }
  const history = useHistory();
  // ====================================================================================

  const requestLogin = (e) => {
    e.preventDefault();
    let mail = localStorage.getItem('userEmail').replace(/"/g, "");
    let pass = localStorage.getItem('userPassword').replace(/"/g, "");

    if (credential.email === mail && credential.password === pass) {
      history.push("/Dashboard");
    }
    else {
      setCredentialError(true)
    }

  }


  const signup=e=>{
    history.push("/Signup")
  }

  const classes = useStyles();

  return (
    <div className={classes.main}>

      <div className={classes.shape}></div>
      <Container maxWidth="xs" className={classes.container}>

        <Typography className={classes.heading} align="center" gutterBottom variant="h4"> Login </Typography>
        <Typography align="center" gutterBottom className={classes.errorEmailText}>

          {
            emailError ? <Errormsg msg="Please enter correct email" /> : null
          }
        </Typography>
        <form action="" onSubmit={requestLogin} >
          <Grid container spacing={2}>
            <Grid item xs={12} sm={12}  >

              <TextField
                autoComplete="off"
                className={classes.inptField}
                variant="outlined"
                label="Email"
                name="email"
                fullWidth
                placeholder="Enter Your Email ID"
                onChange={login}
                value={credential.email}
                onInput={validateEmail}
                color="secondary"
                required
              />

            </Grid>

            <Grid item xs={12} sm={12}  >
              <TextField style={{ marginTop: '10px' }}
                autoComplete="off"
                name="password"
                variant="outlined"
                fullWidth
                label="Password"
                placeholder="Enter Your Password"
                onChange={login}
                value={credential.password}
                color="secondary"
                required
              />
            </Grid>

            <Grid item xs={12} sm={12}  >
              <Button
                className={classes.btnColour}
                variant="contained"
                color="secondary"
                fullWidth
                type="submit"
              // onClick={requestLogin}
              >
                {/* <LockOpenIcon color="secondary" /> */}
                Sign In </Button>
            </Grid>

            <Grid item xs={12} sm={12}  >
              <Button className={classes.btnColour} variant="contained" color="secondary" fullWidth> <FcGoogle />&nbsp; &nbsp;Sign in with Google</Button>
            </Grid>



            <Grid item xs={12} sm={12}  >
              <Button className={classes.NotRegBtn} onClick={signup} fullWidth >Not registered yet..?</Button>
            </Grid>

            <Grid item xs={6} sm={6}  >
              <FormControlLabel
                control={
                  <Checkbox
                    checked={checked}
                    onChange={handleChange}
                    name="checkedA" />}
                label="Remember me"

              />
            </Grid>
            <Grid item xs={6} sm={6}
            >
              <Button variant="text" className={classes.btnColour} color="secondary" fullWidth>Forget Password.?</Button >
            </Grid>
            <Grid item xs={12} sm={12}
            >
              <Typography className={classes.errorEmailText}>

                {
                  credentialerror ? <Errormsg msg="Invalid User / password" /> : null
                }
              </Typography>
            </Grid>

          </Grid>
        </form>
     
      </Container>
      <Typography className={classes.allRight} >@2021 Felix All rights reserved </Typography >
      <div style={{height:'2px'}}>
        <Slider/>
        </div>


    </div>

  )
}
export default Signin;