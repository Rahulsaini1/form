
import React from 'react';

import Signin from './components/Signin';
import { Route, BrowserRouter, Switch } from 'react-router-dom';
import Signup from './components/Signup'
import Dashboard from './components/Dashboard';
import Slider from './components/Slider'

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={Signin} />
          <Route exact path="/Signup" component={Signup} />
          <Route exact path="/Dashboard" component={Dashboard} />
        </Switch>
      </BrowserRouter>

    </div>
  );
}

export default App;
